#include "DynamicObject.h"


//速度。単位は内部単位/フレーム
static const int PLAYER_SPEED = 1000;
static const int ENEMY_SPEED = 500;
static const int HALF_SIZE = 6000;

//内部単位へ
int convertCellToInner(int x) {
	return x * 16000 + 8000;
}
//内部単位から画素単位へ
int convertInnerToPixel(int x) {
	return  (x - 8000 + 500) / 1000;
}

//できるだけ不正くさい値を入れておく。setが呼ばれないと死ぬように。
DynamicObject::DynamicObject() :
	mType(TYPE_NONE),
	mBombPower(2),
	mBombNumber(2),
	mPlayerID(0xffffffff),
	mX(-1),
	mY(-1),
	mDirectionX(0),
	mDirectionY(0) {
	mLastBombX[0] = mLastBombX[1] = -1;
	mLastBombY[0] = mLastBombY[1] = -1;
}

void DynamicObject::set(int x, int y, Type type) {
	srand((unsigned int)time(NULL));
	//内部座標値に変換
	//mX = x * 16000 + 8000;
	//mY = y * 16000 + 8000;

	mX = convertCellToInner(x);
	mY = convertCellToInner(y);

	mType = type;

	//敵専用。移動方向初期化
	if (mType == TYPE_ENEMY) {
		mDirectionX = mDirectionY = 0;
		//Framework f = Framework::instance();
		switch (rand() % 4) {
		case 0: mDirectionX = 1; break;
		case 1: mDirectionX = -1; break;
		case 2: mDirectionY = 1; break;
		case 3: mDirectionY = -1; break;
		}
	}

}

void DynamicObject::draw(Image* image) const {


	//内部座標を画素座標に変換(+500は四捨五入)
	//int dstX = (mX - 8000 + 500) / 1000;
	//int dstY = (mY - 8000 + 500) / 1000;

	int dstX = convertInnerToPixel(mX);
	int dstY = convertInnerToPixel(mY);

	ImageID id = IMAGE_ID_SPACE;
	//画像切り出し位置の同定
	switch (mType) {
	case TYPE_PLAYER:
		switch (mPlayerID) {
			case 0: id = IMAGE_ID_1P; break;
			case 1: id = IMAGE_ID_2P; break;
		}
		break;
	
	case TYPE_ENEMY: id = IMAGE_ID_ENEMY;  break;
		//default: HALT("arienai"); break;
	}
	image->draw(dstX - 144, dstY - 144, id);
}

void DynamicObject::move(GLFWwindow *window, int* wallsX, int *wallsY,int wallNumber) {
	srand((unsigned int)time(NULL));
	/*
	//敵挙動
	if (mType == TYPE_ENEMY) {
		//単位を追ってみよう。
		//mDirectionXは単位なし。dtはミリ秒。ENEMY_SPEEDは画素/秒。
		//この段階ではミリ秒*画素/秒
		//分母に1000をかけると分母と分子がミリ秒になって相殺。
		//画素を内部座標に変えるには1000倍する必要がある。分子と分母で1000が相殺。
		//以上から以下のようになる。
		mX += mDirectionX * ENEMY_SPEED;
		mY += mDirectionY * ENEMY_SPEED;
	}
	else if (mType == TYPE_1P) { //プレイヤー挙動
		int dx, dy;
		dx = dy = 0;

		if (glfwGetKey(window, GLFW_KEY_W) != GLFW_RELEASE) {
			dy = -1;
		}
		else if (glfwGetKey(window, GLFW_KEY_S) != GLFW_RELEASE) {
			dy = 1;
		}
		else if (glfwGetKey(window, GLFW_KEY_A) != GLFW_RELEASE) {
			dx = -1;
		}
		else if (glfwGetKey(window, GLFW_KEY_D) != GLFW_RELEASE) {
			dx = 1;
		}

		mX += dx * PLAYER_SPEED;
		mY += dy * PLAYER_SPEED;
	}
	else if (mType == TYPE_2P) { //プレイヤー挙動
		int dx, dy;
		dx = dy = 0;

		if (glfwGetKey(window, GLFW_KEY_UP) != GLFW_RELEASE) {
			dy = -1;
		}
		else if (glfwGetKey(window, GLFW_KEY_DOWN) != GLFW_RELEASE) {
			dy = 1;
		}
		else if (glfwGetKey(window, GLFW_KEY_LEFT) != GLFW_RELEASE) {
			dx = -1;
		}
		else if (glfwGetKey(window, GLFW_KEY_RIGHT) != GLFW_RELEASE) {
			dx = 1;
		}

		mX += dx * PLAYER_SPEED;
		mY += dy * PLAYER_SPEED;
	}*/
	int dx, dy;

	getVelocity( &dx, &dy, window);

	//mX += dx;
	//mY += dy;

	//X,Y別々に移動した時に当たるかチェック
	int movedX = mX + dx;
	int movedY = mY + dy;
	bool hitX = false;
	bool hitY = false;
	bool hit = false;

	for (int i = 0; i < wallNumber; ++i) {
		if (isIntersectWall(movedX, mY, wallsX[i], wallsY[i])) {
			hitX = hit = true;
		}
		if (isIntersectWall(mX, movedY, wallsX[i], wallsY[i])) {
			hitY = hit = true;
		}
	}
	if (hitX && !hitY) {
		mY = movedY; //Yのみオーケー
	}
	else if (!hitX && hitY) {
		mX = movedX; //Xのみオーケー
	}
	else { //ダメなので普通に
		for (int i = 0; i < wallNumber; ++i) {
			if (isIntersectWall(movedX, movedY, wallsX[i], wallsY[i])) {
				hit = true;
			}
		}
		if (!hit) {
			mX = movedX;
			mY = movedY;
		}
	}


	/*
	//限界処理
	const int X_MIN = 24000;
	const int X_MAX = 320 * 1000 - 40000;
	const int Y_MIN = 24000;
	const int Y_MAX = 240 * 1000 - 24000;
	bool hit = false;
	if (mX < X_MIN) {
		mX = X_MIN;
		hit = true;
	}
	else if (mX > X_MAX) {
		mX = X_MAX;
		hit = true;
	}
	if (mY < Y_MIN) {
		mY = Y_MIN;
		hit = true;
	}
	else if (mY > Y_MAX) {
		mY = Y_MAX;
		hit = true;
	}

	*/

	//敵なら向き変え
	if (hit && mType == TYPE_ENEMY) {
		mDirectionX = mDirectionY = 0;
		switch (rand() % 4) {
		case 0: mDirectionX = 1; break;
		case 1: mDirectionX = -1; break;
		case 2: mDirectionY = 1; break;
		case 3: mDirectionY = -1; break;
		}
	}
	
}


void DynamicObject::getVelocity(int* dx, int* dy, GLFWwindow *window) const {
	//スピードを変数に格納
	int speedX, speedY;
	if (mType == TYPE_ENEMY) {
		speedX = ENEMY_SPEED;
		speedY = ENEMY_SPEED;
	}
	else {
		speedX = PLAYER_SPEED;
		speedY = PLAYER_SPEED;
	}
	//向き取得
	getDirection(dx, dy, window);
	//速度計算
	*dx = *dx * speedX;
	*dy = *dy * speedY;
}

void DynamicObject::getDirection(int* dx, int* dy,GLFWwindow *window) const {
	*dx = *dy = 0;
	if (mType == TYPE_PLAYER) {
		if (mPlayerID == 0) {
			if (glfwGetKey(window, GLFW_KEY_W) != GLFW_RELEASE) {
				*dy = -1;
				std::cout << "W\n";
			}
			else if (glfwGetKey(window, GLFW_KEY_S) != GLFW_RELEASE) {
				*dy = 1;
				std::cout << "S\n";
			}
			else if (glfwGetKey(window, GLFW_KEY_A) != GLFW_RELEASE) {
				*dx = -1;
				std::cout << "A\n";
			}
			else if (glfwGetKey(window, GLFW_KEY_D) != GLFW_RELEASE) {
				*dx = 1;
				std::cout << "D\n";
			}
		}
		else if (mPlayerID == 1) {
			if (glfwGetKey(window, GLFW_KEY_UP) != GLFW_RELEASE) {
				*dy = -1;
				std::cout << "UP\n";
			}
			else if (glfwGetKey(window, GLFW_KEY_DOWN) != GLFW_RELEASE) {
				*dy = 1;
				std::cout << "DOWN\n";
			}
			else if (glfwGetKey(window, GLFW_KEY_LEFT) != GLFW_RELEASE) {
				*dx = -1;
				std::cout << "LEFT\n";
			}
			else if (glfwGetKey(window, GLFW_KEY_RIGHT) != GLFW_RELEASE) {
				*dx = 1;
				std::cout << "RIGHT\n";
			}
		}
	}
	else if (mType == TYPE_ENEMY) {
		*dx = mDirectionX;
		*dy = mDirectionY;
	}
}

void DynamicObject::getCell(int* x, int* y) const {
	*x = mX / 16000;
	*y = mY / 16000;
}


bool DynamicObject::isIntersectWall(int wallX, int wallY) {
	return isIntersectWall(mX, mY, wallX, wallY);
}

void DynamicObject::doCollisionReactionToDynamic(DynamicObject* another) {
	//相手が死んでいれば無視
	if (another->isDead()) {
		return;
	}
	//対称に書くために別名に格納
	DynamicObject& o1 = *this;
	DynamicObject& o2 = *another;

	
	if (o1.isIntersect(o2)) { //衝突判定
							  //プレイヤーを殺す
		if (o1.isPlayer() && o2.isEnemy()) {
			o1.die();
		}
		else if (o1.isEnemy() && o2.isPlayer()) {
			o2.die();
		}
	}
	
}

bool DynamicObject::isIntersect(const DynamicObject& o) const {
	int al = mX - HALF_SIZE; //left A
	int ar = mX + HALF_SIZE; //right A
	int bl = o.mX - HALF_SIZE; //left B
	int br = o.mX + HALF_SIZE; //right B
	if ((al < br) && (ar > bl)) {
		int at = mY - HALF_SIZE; //top A
		int ab = mY + HALF_SIZE; //bottom A
		int bt = o.mY - HALF_SIZE; //top B
		int bb = o.mY + HALF_SIZE; //bottom B
		if ((at < bb) && (ab > bt)) {
			return true;
		}
	}
	return false;

}

bool DynamicObject::isIntersectWall(int x, int y, int wallX, int wallY) {
	int wx = convertCellToInner(wallX);
	int wy = convertCellToInner(wallY);

	int al = x - HALF_SIZE; //left A
	int ar = x + HALF_SIZE; //right A
	int bl = wx - 8000; //left B
	int br = wx + 8000; //right B
	if ((al < br) && (ar > bl)) {
		int at = y - HALF_SIZE; //top A
		int ab = y + HALF_SIZE; //bottom A
		int bt = wy - 8000; //top B
		int bb = wy + 8000; //bottom B
		if ((at < bb) && (ab > bt)) {
			return true;
		}
	}
	return false;
}

bool DynamicObject::hasBombButtonPressed(GLFWwindow *window) const {
	if (mType == TYPE_PLAYER) {
		if (mPlayerID == 0) {
			//std::cout << "G\n";
			return (glfwGetKey(window, GLFW_KEY_G) != GLFW_RELEASE);
		}
		else if (mPlayerID == 1) {
			//std::cout << "M\n";
			return (glfwGetKey(window, GLFW_KEY_M) != GLFW_RELEASE);
		}
	}
	return false;
}

bool DynamicObject::isPlayer() const {
	return (mType == TYPE_PLAYER);
}

bool DynamicObject::isEnemy() const {
	return (mType == TYPE_ENEMY);
}

void DynamicObject::die() {
	PlaySound("Sound/tin1.wav", NULL, SND_FILENAME | SND_ASYNC);
	mType = TYPE_NONE;
}

bool DynamicObject::isDead() const {
	return (mType == TYPE_NONE);
}