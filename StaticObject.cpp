#include "StaticObject.h"
#include "Image.h"

//デフォルト壁。壁にしておけば抜けることはない。
StaticObject::StaticObject() : 
mCount(0),
mBombOwner(0),
mFlags(0) {
}

void StaticObject::setFlag(unsigned f) {
	mFlags |= f;
}

void StaticObject::resetFlag(unsigned f) {
	mFlags &= ~f;
}

bool StaticObject::checkFlag(unsigned f) const {
	return (mFlags & f) ? true : false;
}

void StaticObject::draw(int x, int y, Image* image) {


	ImageID id = IMAGE_ID_SPACE;

	bool floor = false;
	if (mFlags & FLAG_WALL) {
		id = IMAGE_ID_WALL;
	}
	else if (mFlags & FLAG_BRICK) {
		id = IMAGE_ID_BRICK;
	}else {
		id = IMAGE_ID_FLOOR;
		floor = true;
	}

	if (mFlags & FLAG_BURNBRICK) {
		id = IMAGE_ID_BURNBRICK;
	}
	image->draw(x * 16 - 144, y * 16 - 144, id);


	//オプション描画

	if (floor) {

		id = IMAGE_ID_SPACE;
		if ((mFlags & FLAG_BOMB) && !(mFlags & FLAG_EXPLODING)) {
			id = IMAGE_ID_BOMB;
		}else if (mFlags & FLAG_ITEM_BOMB) {
			id = IMAGE_ID_ITEM_BOMB;
		}
		else if (mFlags & FLAG_ITEM_POWER) {
			id = IMAGE_ID_ITEM_POWER;
		}
		if (id != IMAGE_ID_SPACE) {
			image->draw(x * 16 - 144, y * 16 - 144, id);
		}
	}


}

void StaticObject::drawExplosion(int x, int y, Image* image) {
	
	ImageID id = IMAGE_ID_SPACE;
	
	if (!(mFlags & FLAG_WALL) && !(mFlags & FLAG_BRICK)) { //壁の上には爆風は描かない
		if (mFlags & FLAG_EXPLODING) {
			id = IMAGE_ID_EXPLODING;
		}
		else if (mFlags & FLAG_FIRE_X) {
			if (mFlags & FLAG_FIRE_Y) {
				id = IMAGE_ID_EXPLODING;
			}
			else {
				id = IMAGE_ID_FIRE_X;
			}
		}
		else if (mFlags & FLAG_FIRE_Y) {
			id = IMAGE_ID_FIRE_Y;
		}
	}
	if (id != IMAGE_ID_SPACE) {
		image->draw(x * 16 - 144, y * 16 - 144, id);
	}
}
