#ifndef INCLUDED_GAME_DYNAMIC_OBJECT_H
#define INCLUDED_GAME_DYNAMIC_OBJECT_H

#include "Image.h"
#include "time.h"
#include "mmsystem.h"

class Image;

class DynamicObject {
public:

	enum Type {
		//TYPE_1P,
		//TYPE_2P,
		TYPE_PLAYER,
		TYPE_ENEMY,

		TYPE_NONE, //死亡
	};

	//マス描画関数
	enum ImageID {
		IMAGE_ID_1P = 20, //1P
		IMAGE_ID_2P = 21, //2P
		IMAGE_ID_ENEMY = 22, //ENEMY
		IMAGE_ID_SPACE = 3 //Unknow
	};

	DynamicObject();
	void set(int x, int y, Type);
	void draw(Image*) const;
	void move(GLFWwindow *window, int *wallsX,int *wallsY, int wallNumber);

	//どこのマスに中心があるのかたずねる。
	void getCell(int* x, int* y) const;


	//壁用
	bool isIntersectWall(int wallCellX, int wallCellY);
	void doCollisionReactionToDynamic(DynamicObject* another);
	bool isIntersect(const DynamicObject& o) const;

	//便利関数群
	bool hasBombButtonPressed(GLFWwindow *window) const;	//爆弾ボタンが押されたか調べる
	bool isPlayer() const;
	bool isEnemy() const;
	void die(); //死にます(mTypeをNONEにすることで表現)
	bool isDead() const; //死んでますか？

	int mPlayerID;//プレイヤー番号
	//プレイヤー専用
	int mBombPower; //爆発力
	int mBombNumber; //爆弾数
	

	Type mType;
	int mX;
	int mY;
	//敵専用
	int mDirectionX;
	int mDirectionY;

	//爆弾を置く人専用
	int mLastBombX[2];
	int mLastBombY[2];

private:
	//今フレームの移動量を取得
	void getVelocity(int* dx, int* dy,GLFWwindow *window) const;
	//移動方向を取得
	void getDirection(int* dx, int* dy,GLFWwindow *window) const;
	//壁用
	static bool isIntersectWall(int x, int y, int wallCellX, int wallCellY);
};

#endif