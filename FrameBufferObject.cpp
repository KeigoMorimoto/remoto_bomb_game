#include "FrameBufferObject.h"

FramebufferObject::FramebufferObject()
{
	cbWidth = 0;
	cbHeight = 0;
	fbID = 0;
	cbID = 0;
}

FramebufferObject::~FramebufferObject()
{
}

void FramebufferObject::Initialize(int width, int height)
{
	cbWidth = width;
	cbHeight = height;

	glGenTextures(1, &cbID);
	glBindTexture(GL_TEXTURE_2D, cbID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, cbWidth, cbHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	// フレームバッファオブジェクトを作成する
	glGenFramebuffersEXT(1, &fbID);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbID);

	// フレームバッファオブジェクトにカラーバッファとしてテクスチャを結合する
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, cbID, 0);

	// フレームバッファオブジェクトの結合を解除する
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
}

void FramebufferObject::Shutdown()
{
	glDeleteFramebuffersEXT(1, &fbID);
	glDeleteRenderbuffersEXT(1, &cbID);
	//glDeleteTextures(1, &texID);

	fbID = 0;
	cbID = 0;

}