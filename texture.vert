#version 120
//
// shader.vert
//

//attribute vec3 normal;

uniform mat4 MVP;

attribute vec3 position;
attribute vec2 uv;

varying vec2 vuv;
//varying vec4 vColor;

void main(void){
     gl_Position = MVP * vec4(position, 1.0);
     vuv = uv;
}