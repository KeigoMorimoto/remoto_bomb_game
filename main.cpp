﻿#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <gl/glew.h>
#include <GLFW/glfw3.h>
#include <vector>
#include <windows.h>
#include "Fps.h"
#include "State.h"
#include "Image.h"
#include "File.h"
#include "StaticObject.h"

// glmの使う機能をインクルード
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
//using namespace glm;でもいいけどここでは一部のみ「glm::」を省力できるようにする
using glm::vec3;
using glm::vec4;
using glm::mat4;

GLFWwindow* initGLFW(int width, int height)
{
	// GLFW初期化
	if (glfwInit() == GL_FALSE)
	{
		return nullptr;
	}
	// ウィンドウ生成
	GLFWwindow* window = glfwCreateWindow(width, height, "Bomb_Game", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return nullptr;
	}
	// バージョン2.1指定
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(0);
	// GLEW初期化
	if (glewInit() != GLEW_OK)
	{
		return nullptr;
	}
	return window;
}

int main()
{

	//Image.h側も変更する
	GLint width = 800, height = 600;
	Fps fps;
	GLFWwindow* window = initGLFW(width, height);

	// タイマーのセッティング
	double FPS = 60.0; // <--- 一秒間に更新する回数(30 か 60) 
	double currentTime, lastTime, elapsedTime, sleepTime;
	currentTime = lastTime = elapsedTime = sleepTime = 0.0;
	glfwSetTime(0.0); // <--- タイマーを初期化する

					  /* 光源の初期設定 */
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	GLfloat light0pos[] = { 0.0, 3.0, 5.0, 1.0 };

	//glEnable(GL_CULL_FACE);

	double time = 0;

	State *state = new State(0);

	bool reentrant = true;

	// フレームループ
	while (glfwWindowShouldClose(window) == GL_FALSE)
	{
		currentTime = glfwGetTime();
		elapsedTime = currentTime - lastTime;
		sleepTime = 16.6 - elapsedTime;
		

		//fps.Updata();
		//fps.Wait();

		if (elapsedTime >= 1.0 / FPS) {

			//count ++;

			//glUseProgram(shader);
			glEnable(GL_DEPTH_TEST);
			//glDepthFunc(GL_LESS);
			glClearColor(0.2f, 0.2f, 0.2f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			
			if (reentrant == true) {
				state->update(window);
			}
			state->draw();

			if (glfwGetKey(window, GLFW_KEY_P) != GLFW_RELEASE) {
				reentrant = false;
			}

			if (glfwGetKey(window, GLFW_KEY_O) != GLFW_RELEASE) {
				reentrant = true;
			}
			

			time++;
			lastTime = glfwGetTime();

			// ダブルバッファのスワップ
			glfwSwapBuffers(window);
			glfwPollEvents();
		}
		Sleep(sleepTime);

	}
	// GLFWの終了処理
	glfwTerminate();

	SAFE_DELETE(state);

	return 0;
}