#ifndef INCLUDE_MACRO_H
#define INCLUDE_MACRO_H

//マクロ類
#define STRONG_ASSERT( exp ) ( ( !! ( exp ) ) || ( GameLib::halt( __FILE__, __LINE__, #exp ), 0 ) ) //この行の意味を理解するのは相当難しい
#define HALT( exp ) { GameLib::halt( __FILE__, __LINE__, #exp ); }
#define SAFE_DELETE( x ) { delete ( x ); ( x ) = 0; }
#define SAFE_DELETE_ARRAY( x ) { delete[] ( x ); ( x ) = 0; }

//デバグとリリースで分岐するもの
#ifndef NDEBUG
#define ASSERT( exp ) ( ( !! ( exp ) ) || ( GameLib::halt( __FILE__, __LINE__, #exp ), 0 ) )
#else //NDEBUG
#define ASSERT( exp )
#endif //NDEBUG

#endif