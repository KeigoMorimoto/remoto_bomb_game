#ifndef INCLUDED_STATE_H
#define INCLUDED_STATE_H

#include "Array2D.h"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <gl/glew.h>
#include <GLFW/glfw3.h>
#include "Image.h"
#include "StaticObject.h"
// glmの使う機能をインクルード
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "macro.h"
#include "DynamicObject.h"
#include <time.h>
#include "mmsystem.h"

using glm::vec3;
using glm::vec4;
using glm::mat4;

class Image; 

class State {
public:
	//ステージ番号を与えてコンストラクト。0だと二人用
	static const int STAGE_ID_2PLAYERS = 0;
	State(int stageID);
	~State();
	//入力は中でとる
	void update(GLFWwindow *window);
	void draw();
	//外から取る情報
	//bool hasCleared() const;
	//bool isAlive1P() const; //1P生きてる？
	//bool isAlive2P() const; //2P生きてる？

	//bool isAlive(int playerID) const; //生きてる？
private:
	//クリア判定
	bool cleared = false;
	int clearedID = -1;
	//クリア経過時間
	int tooktime = 0;
	
	//敵のアイテムドロップ
	bool EnemyDownFrag = false;
	int EnemyItemX = -1;
	int EnemyItemY = -1;
	int item_rand = 0;

	int EnemyDownTime = 0;


	//炎設置。座標は爆弾の座標
	void setFire(int x, int y);


	//動かないオブジェクト
	Array2D< StaticObject > mStaticObjects;
	//動くオブジェクト
	DynamicObject* mDynamicObjects;
	int mDynamicObjectNumber;

	int mStageID;

	Image* mImage = new Image(); //画像
};

#endif