#ifndef _FRAMEBUFFER_OBJECT_H_INCLUDED_
#define _FRAMEBUFFER_OBJECT_H_INCLUDED_

#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>


#define FBOWIDTH  800      // フレームバッファオブジェクトの幅
#define FBOHEIGHT 600       // フレームバッファオブジェクトの高さ


//////////////////////////////////////////////////////////////////////////
//　　FramebufferObject
//////////////////////////////////////////////////////////////////////////
class FramebufferObject
{
protected:
	int cbWidth;
	int cbHeight;
	GLuint fbID;
	GLuint cbID;
	//GLuint texID;

public:
	FramebufferObject();
	~FramebufferObject();

	void Initialize(int width, int height);
	//void Render(void(*render_func)(void));
	void Shutdown();

	int GetWidth() { return cbWidth; }
	int GetHeight() { return cbHeight; }
	GLuint GetFramebufferID() { return fbID; }
	GLuint GetColorbufferID() { return cbID; }
	//GLuint GetTextureID() { return texID; }
};




#endif  //　_FRAMEBUFFER_OBJECT_H_INCLUDED_