#version 120
//
// shader.frag
//
varying vec2 vuv;
uniform sampler2D texture;
uniform float time;

float wave(vec2 uv, vec2 emitter, float speed, float phase){
	float dst = distance(uv, emitter);
	return pow((0.5 + 0.5 * sin(dst * phase - time * speed)), 2.0);
}

void main(void){
	//vec2 tFrag = vec2(1.0 / 512.0);

  float vignet = length(vuv - vec2(0.5,0.5));


  vec2 pos = vuv;

  float distortion = 0.05; 
  pos -= vec2(0.5, 0.5);
  pos *= vec2(pow(length(pos), distortion));
  pos += vec2(0.5, 0.5);

	
  vec3 color = texture2D(texture, pos).rgb;

  color.r = texture2D(texture, pos).r;
  color.g = texture2D(texture, pos - vec2(0.002, 0)).g;
  color.b = texture2D(texture, pos - vec2(0.004, 0)).b;



  /*
  float gamma = 1.5;
  color.r = pow(color.r, gamma);
  color.g = pow(color.g, gamma);
  color.b = pow(color.b, gamma);

  vec3 col1 = vec3(0.612, 0.725, 0.086);
  vec3 col2 = vec3(0.549, 0.667, 0.078);
  vec3 col3 = vec3(0.188, 0.392, 0.188);
  vec3 col4 = vec3(0.063, 0.247, 0.063);	

  float dist1 = length(color - col1);
  float dist2 = length(color - col2);
  float dist3 = length(color - col3);
  float dist4 = length(color - col4);
  
  float d = min(dist1, dist2);
  d = min(d, dist3);
  d = min(d, dist4);

  if (d == dist1) {
    color = col1;
  }    
  else if (d == dist2) {
    color = col2;
  }    
  else if (d == dist3) {
    color = col3;
  }    
  else {
    color = col4;
  }
  */
  color -= abs(sin(vuv.y * 100.0 - time * 5.0)) * 0.08; // (1)
  color -= abs(sin(vuv.y * 300.0 + time * 10.0)) * 0.05; // (2)
  color -= abs(sin(vuv.y * 10.0 + time * 1.0)) * 0.1; // (2)
  color *= 1 - vignet * 1.3;

  gl_FragColor = vec4(color, 1.0).rgba;
}