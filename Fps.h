#ifndef INCLUDE_FPS_H
#define INCLUDE_FPS_H


class Fps {
public:

	int mStartTime;         //測定開始時刻
	int mCount;             //カウンタ
	float mFps;             //fps
	static const int N = 10;//平均を取るサンプル数
	static const int FPS = 60;	//設定したFPS  
	Fps();

	bool Updata();
	void Draw();
	void Wait();

};
#endif // !INCLUDE_FPS_H