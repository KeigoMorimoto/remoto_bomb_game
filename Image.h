﻿#ifndef INCLUDED_IMAGE_H
#define INCLUDED_IMAGE_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <gl/glew.h>
#include <GLFW/glfw3.h>
#include <vector>
#include <windows.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "FrameBufferObject.h"
#include "OBJLoader.h"

//#define FBOWIDTH        // フレームバッファオブジェクトの幅
//#define FBOHEIGHT        // フレームバッファオブジェクトの高さ

using glm::vec3;
using glm::vec4;
using glm::mat4;

class Image {
public:
	Image();
	~Image();

	void draw(int X,int Y,int ID);
	void drawcleared(int ID);
	

private:

	double animtime = 0;

	GLint width = 800, height = 600;
	GLint simple_shader;
	GLint texture_shader;
	GLint game_shader;
	GLint fire_shader;
	GLint water_shader;

	GLuint texID[8];
	GLuint matrixID;
	GLuint simple_matrixID;

	int positionLocation;
	int simple_positionLocation;
	int simple_colorLocation;
	int uvLocation;
	int textureLocation;
	int timeLocation2;
	int timeLocation3;
	int timeLocation4;
	int useDoublefireLocation;

	FramebufferObject fbo;

	OBJMesh mesh;
	

	GLfloat player_vertex_position[15] = {
		0.0f,8.0f,0.0f,
		8.0f, 0.0f,-8.0f,
		-8.0f, 0.0f,-8.0f,
		-8.0f, 0.0f,8.0f,
		8.0f, 0.0f,8.0f
	};

	GLfloat player_color[20] = {
		0.3f, 0.3f, 0.3f,0.0f,
		0.0f, 0.0f, 0.0f,0.0f,
		0.0f, 0.0f, 0.0f,0.0f,
		0.0f, 0.0f, 0.0f,0.0f,
		0.0f, 0.0f, 0.0f,0.0f,
	};

	GLuint player_index[18] = {
		0,1,2,
		0,2,3,
		0,3,4,
		0,4,1,
		2,1,3,
		1,4,2
	};

	GLfloat block_vertex_position[15] = {
		0.0f,16.0f,0.0f,
		8.0f, 0.0f,-8.0f,
		-8.0f, 0.0f,-8.0f,
		-8.0f, 0.0f,8.0f,
		8.0f, 0.0f,8.0f
	};

	GLfloat block_color[20] = {
		1.0f, 1.0f, 1.0f,0.0f,
		0.3f, 0.3f, 0.3f,0.0f,
		0.3f, 0.3f, 0.3f,0.0f,
		0.3f, 0.3f, 0.3f,0.0f,
		0.3f, 0.3f, 0.3f,0.0f,
	};

	GLuint block_index[18] = {
		0,1,2,
		0,2,3,
		0,3,4,
		0,4,1,
		2,1,3,
		1,4,2
	};

	/***1P***/
	GLfloat oneP_position[15] = {
		0.0f,8.0f,0.0f,
		8.0f, 0.0f,-8.0f,
		-8.0f, 0.0f,-8.0f,
		-8.0f, 0.0f,8.0f,
		8.0f, 0.0f,8.0f
	};

	GLfloat oneP_color[20] = {
		0.5f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
	};

	GLuint oneP_index[18] = {
		0,1,2,
		0,2,3,
		0,3,4,
		0,4,1,
		2,1,3,
		1,4,2
	};

	/***2P***/
	GLfloat twoP_position[15] = {
		0.0f,8.0f,0.0f,
		8.0f, 0.0f,-8.0f,
		-8.0f, 0.0f,-8.0f,
		-8.0f, 0.0f,8.0f,
		8.0f, 0.0f,8.0f
	};

	GLfloat twoP_color[20] = {
		0.0f, 0.5f, 0.5f, 1.0f,
		0.0f, 1.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f, 1.0f,
	};

	GLuint twoP_index[18] = {
		0,1,2,
		0,2,3,
		0,3,4,
		0,4,1,
		2,1,3,
		1,4,2
	};

	/***Wall***/
	GLfloat wall_vertex_position[24] = {
		-8.0f,8.0f,-8.0f,
		8.0f,8.0f,-8.0f,
		-8.0f,8.0f,8.0f,
		8.0f,8.0f,8.0f,

		-8.0f, 0.0f,8.0f,
		8.0f, 0.0f,8.0f,
		8.0f, 0.0f,-8.0f,
		-8.0f, 0.0f,-8.0f
	};

	GLfloat wall_color[32] = {
		0.0f, 0.5f, 0.0f, 1.0f,
		0.0f, 0.5f, 0.0f, 1.0f,
		0.0f, 0.5f, 0.0f, 1.0f,
		0.0f, 0.5f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
	};

	GLuint wall_index[36] = {
		0,2,1,
		1,2,3,

		2,4,3,
		3,4,5,

		3,5,1,
		1,5,6,

		1,6,7,
		0,1,7,

		0,7,2,
		2,7,4,

		4,7,5,
		5,7,6
	};

	/***BRICK***/
	GLfloat brick_vertex_position[15] = {
		0.0f,8.0f,0.0f,
		8.0f, 0.0f,-8.0f,
		-8.0f, 0.0f,-8.0f,
		-8.0f, 0.0f,8.0f,
		8.0f, 0.0f,8.0f
	};
	
	GLfloat brick_color[20] = {
		0.0f, 0.0f, 0.5f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
	};
	
	GLuint brick_index[18] = {
		0,1,2,
		0,2,3,
		0,3,4,
		0,4,1,
		2,1,3,
		1,4,2
	};

	GLfloat burnbrick_color[20] = {
		0.0f, 0.0f, 0.5f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
	};
	/*
	GLfloat brick_color[32] = {
		0.2f, 0.2f, 0.2f, 1.0f,
		0.2f, 0.2f, 0.2f, 1.0f,
		0.2f, 0.2f, 0.2f, 1.0f,
		0.2f, 0.2f, 0.2f, 1.0f,
		0.2f, 0.2f, 0.2f, 1.0f,
		0.2f, 0.2f, 0.2f, 1.0f,
		0.2f, 0.2f, 0.2f, 1.0f,
		0.2f, 0.2f, 0.2f, 1.0f,
	};
	*/

	/***TEXTURE***/
	// 頂点データ
	float vertex_position[12] = {
		8.0f, 0.0f,-8.0f,
		-8.0f, 0.0f,-8.0f,
		-8.0f, 0.0f,8.0f,
		8.0f, 0.0f,8.0f
	};

	const GLfloat vertex_uv[8] = {
		1, 0,
		0, 0,
		0, 1,
		1, 1,
	};


	/***BOMB***/
	
	GLfloat bomb_vertex_position[24] = {
		-4.0f,4.0f,-4.0f,
		4.0f,4.0f,-4.0f,
		-4.0f,4.0f,4.0f,
		4.0f,4.0f,4.0f,

		-4.0f, 0.0f,4.0f,
		4.0f, 0.0f,4.0f,
		4.0f, 0.0f,-4.0f,
		-4.0f, 0.0f,-4.0f
	};

	GLfloat bomb_color[32] = {
		1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
		

		1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f,
		
	};

	GLuint bomb_index[36] = {
		0,2,1,
		1,2,3,

		2,4,3,
		3,4,5,

		3,5,1,
		1,5,6,

		1,6,7,
		0,1,7,

		0,7,2,
		2,7,4,

		4,7,5,
		5,7,6
	};
	
	float bomb_tx_position[12] = {
		4.0f, 4.0f,-4.0f,
		-4.0f, 4.0f,-4.0f,
		-4.0f, 4.0f,4.0f,
		4.0f, 4.0f,4.0f
	};

	const GLfloat bomb_tx_uv[8] = {
		1, 0,
		0, 0,
		0, 1,
		1, 1,
	};
	
	/*
	GLfloat bomb_vertex_position[16] = {
		-4.0f, 6.0f,3.4641f,
		4.0f, 6.0f,3.461f,
		0.0f, 6.0f,-3.461f,
		0.0f, 12.0f,0.0f,

		0.0f, 0.0f,0.0f,
	};

	GLfloat bomb_color[20] = {
		1.0f, 0.647f, 0.0f, 1.0f,
		1.0f, 0.647f, 0.0f, 1.0f,
		1.0f, 0.647f, 0.0f, 1.0f,
		1.0f, 0.647f, 0.0f, 1.0f,
		1.0f, 0.647f, 0.0f, 1.0f
	};

	GLuint bomb_index[21] = {
		0,1,2,
		3,0,1,

		3,1,2,
		3,2,0,
		4,1,2,
		4,2,1,
		4,0,2
	};
	*/
	


	/***FIRE_X***/
	/***FIRE_Y***/
	/***FIRE_ECPLODING***/

	/***FRAME_BUFFER***/
	float f_position[12] = {
		-1.0f, 1.0f,0.0f,
		1.0f, 1.0f,0.0f,
		1.0f, -1.0f,0.0f,
		-1.0f, -1.0f,0.0f,
		
	};

	const GLfloat f_uv[8] = {
		0, 1,
		1, 1,
		1, 0,
		0, 0,
		
	};
	mat4 modelMat;
	mat4 mvpMat;

	// View行列を計算
	mat4 viewMat = glm::lookAt(
		vec3(0.0, 300.0, 150.0), // ワールド空間でのカメラの座標
		vec3(0.0, 0.0, 0.0), // 見ている位置の座標
		vec3(0.0, 1.0, 0.0)  // 上方向を示す。(0,1.0,0)に設定するとy軸が上になります
	);

	// Projection行列を計算
	mat4 projectionMat = glm::perspective(
		glm::radians(45.0f), // ズームの度合い(通常90〜30)
		(GLfloat)width / (GLfloat)height,		// アスペクト比
		0.1f,		// 近くのクリッピング平面
		1000.0f		// 遠くのクリッピング平面
	);

	// View行列を計算
	mat4 orthoMat = glm::lookAt(
		vec3(0.0, 0.0, 3.0), // ワールド空間でのカメラの座標
		vec3(0.0, 0.0, 0.0), // 見ている位置の座標
		vec3(0.0, 1.0, 0.0)  // 上方向を示す。(0,1.0,0)に設定するとy軸が上になります
	);

	// View行列を計算
	mat4 orthoMat1 = glm::orthoLH(
		-1.0,1.0,-1.0,1.0,2.0,4.0
	);

};
#endif


