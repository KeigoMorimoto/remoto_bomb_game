#ifndef INCLUDED_GAME_STATIC_OBJECT_H
#define INCLUDED_GAME_STATIC_OBJECT_H

class Image;
class DynamicObject;

class StaticObject {
public:
	enum Flag {
		FLAG_WALL = (1 << 0), //コンクリ
		FLAG_BRICK = (1 << 1), //レンガ
		FLAG_ITEM_BOMB = (1 << 2), //爆弾アイテム
		FLAG_ITEM_POWER = (1 << 3), //爆風アイテム
		FLAG_BOMB = (1 << 4), //爆弾
		FLAG_FIRE_X = (1 << 5), //横方向炎
		FLAG_FIRE_Y = (1 << 6), //縦方向炎
		FLAG_EXPLODING = (1 << 7), //爆発中
		FLAG_BURNBRICK = (1 << 8), //燃えてるレンガ
	};

	//マス描画関数
	enum ImageID {
		IMAGE_ID_WALL = 0,	//壁
		IMAGE_ID_BRICK = 1, //レンガ
		IMAGE_ID_FLOOR = 2, //床
		IMAGE_ID_SPACE = 3, //Unknow
		IMAGE_ID_ITEM_BOMB = 4, //爆弾アイテム
		IMAGE_ID_ITEM_POWER = 5, //爆風アイテム
		IMAGE_ID_BOMB = 6,	//爆弾
		IMAGE_ID_FIRE_X = 7,	//横方向炎
		IMAGE_ID_FIRE_Y = 8,	//縦方向炎
		IMAGE_ID_EXPLODING = 9,	//爆発中
		IMAGE_ID_BURNBRICK = 10,	//燃えてるレンガ
	};


	StaticObject();

	//床、壁、煉瓦を描画し、爆弾やアイテムがあればそれも描画
	void draw(int x, int y, Image*);

	bool checkFlag(unsigned) const;
	void setFlag(unsigned);
	void resetFlag(unsigned);

	//爆風を描画
	void drawExplosion(int x, int y, Image* image);

	int mCount;	//何かのカウント(爆弾設置、爆発、焼かれ始め)

	DynamicObject* mBombOwner; //爆弾の持ち主

private:
	unsigned mFlags;
};

#endif