#include "Fps.h"
#include "GLFW/glfw3.h"
#include <math.h>
#include <Windows.h>

Fps::Fps() : mStartTime(0), mCount(0), mFps(0) {}

bool Fps::Updata() {

	if (mCount == 0) { //1フレーム目なら時刻を記憶
		mStartTime = glfwGetTime();
	}

	if (mCount == N) { //60フレーム目なら平均を計算する
		int t = glfwGetTime();
		mFps = 1000.f / ((t - mStartTime) / (float)N);
		mCount = 0;
		mStartTime = t;
	}
	mCount++;
	return true;
}

void Fps::Draw() {
	//DrawFormatString(500, 400, GetColor(255, 255, 255), "fps%.1f", mFps);
}

void Fps::Wait() {
	int tookTime = glfwGetTime() - mStartTime;	//かかった時間
	int waitTime = mCount * 1000 / FPS - tookTime;	//待つべき時間
	if (waitTime > 0) {
		Sleep(waitTime);	//待機
	}
}