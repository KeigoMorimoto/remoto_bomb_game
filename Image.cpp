#include "Image.h"

GLfloat lightColor[4] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat lightPosition[4] = { 0.0, 3.0, 5.0, 1.0 };

void SetLighting()
{
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	//glLightfv(GL_LIGHT0, GL_AMBIENT, lightColor);
}

int readShaderSource(GLuint shaderObj, std::string fileName)
{
	//ファイルの読み込み
	std::ifstream ifs(fileName);
	if (!ifs)
	{
		std::cout << "error" << std::endl;
		return -1;
	}
	std::string source;
	std::string line;
	while (getline(ifs, line))
	{
		source += line + "\n";
	}
	// シェーダのソースプログラムをシェーダオブジェクトへ読み込む
	const GLchar *sourcePtr = (const GLchar *)source.c_str();
	GLint length = source.length();
	glShaderSource(shaderObj, 1, &sourcePtr, &length);
	return 0;
}
GLint makeShader(std::string vertexFileName, std::string fragmentFileName)
{
	// シェーダーオブジェクト作成
	GLuint vertShaderObj = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragShaderObj = glCreateShader(GL_FRAGMENT_SHADER);
	GLuint shader;
	// シェーダーコンパイルとリンクの結果用変数
	GLint compiled, linked;
	/* シェーダーのソースプログラムの読み込み */
	if (readShaderSource(vertShaderObj, vertexFileName)) return -1;
	if (readShaderSource(fragShaderObj, fragmentFileName)) return -1;
	/* バーテックスシェーダーのソースプログラムのコンパイル */
	glCompileShader(vertShaderObj);
	glGetShaderiv(vertShaderObj, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE)
	{
		fprintf(stderr, "Compile error in vertex shader.\n");
		return -1;
	}
	/* フラグメントシェーダーのソースプログラムのコンパイル */
	glCompileShader(fragShaderObj);
	glGetShaderiv(fragShaderObj, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE)
	{
		fprintf(stderr, "Compile error in fragment shader.\n");
		return -1;
	}
	/* プログラムオブジェクトの作成 */
	shader = glCreateProgram();
	/* シェーダーオブジェクトのシェーダープログラムへの登録 */
	glAttachShader(shader, vertShaderObj);
	glAttachShader(shader, fragShaderObj);
	/* シェーダーオブジェクトの削除 */
	glDeleteShader(vertShaderObj);
	glDeleteShader(fragShaderObj);
	/* シェーダープログラムのリンク */
	glLinkProgram(shader);
	glGetProgramiv(shader, GL_LINK_STATUS, &linked);
	if (linked == GL_FALSE)
	{
		fprintf(stderr, "Link error.\n");
		return -1;
	}
	return shader;
}
GLuint loadTexture(std::string filename)
{
	// テクスチャIDの生成
	GLuint texID;
	glGenTextures(1, &texID);

	// ファイルの読み込み
	std::ifstream fstr(filename, std::ios::binary);
	const size_t fileSize = static_cast<size_t>(fstr.seekg(0, fstr.end).tellg());
	fstr.seekg(0, fstr.beg);
	char* textureBuffer = new char[fileSize];
	fstr.read(textureBuffer, fileSize);

	// テクスチャをGPUに転送
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 256, 256, 0, GL_RGB, GL_UNSIGNED_BYTE, textureBuffer);

	// テクスチャの設定
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// テクスチャのアンバインド
	delete[] textureBuffer;
	glBindTexture(GL_TEXTURE_2D, 0);

	return texID;
}
/*4つ目の引数いらない*/
void drawTexture(GLuint textureLocation, GLuint positionLocation, GLuint uvLocation, GLuint texID, float *vertex_position, const GLfloat *vertex_uv) {

	// uniform属性の登録(おまじない)
	//glUniform1i(textureLocation, 0);

	// attribute属性を登録
	glVertexAttribPointer(positionLocation, 3, GL_FLOAT, false, 0, vertex_position);
	glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 0, vertex_uv);

	// モデルの描画
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
};


Image::Image() {
	texture_shader = makeShader("texture.vert", "texture.frag");
	simple_shader = makeShader("simple.vert", "simple.frag");
	game_shader = makeShader("texture.vert", "GameBoy.frag");
	fire_shader = makeShader("texture.vert", "fire.frag");
	water_shader = makeShader("texture.vert", "water.frag");

	/***texture.vert,fire.frag***/
	matrixID = glGetUniformLocation(texture_shader, "MVP");
	simple_matrixID = glGetUniformLocation(simple_shader, "MVP");

	positionLocation = glGetAttribLocation(texture_shader, "position");
	uvLocation = glGetAttribLocation(texture_shader, "uv");
	textureLocation = glGetUniformLocation(texture_shader, "texture");

	/***simple.vert,simple.frag***/
	simple_positionLocation = glGetAttribLocation(simple_shader, "position");
	simple_colorLocation = glGetAttribLocation(simple_shader, "color");

	/***texture.vert,game.frag***/
	timeLocation2 = glGetUniformLocation(game_shader, "time");
	//useLocation = glGetUniformLocation(game_shader, "useShader");

	/***texture.vert,fire.frag***/
	timeLocation3 = glGetUniformLocation(fire_shader, "time");
	useDoublefireLocation = glGetUniformLocation(fire_shader, "x");

	/***texture.vert,water.frag***/
	timeLocation4 = glGetUniformLocation(water_shader, "time");

	glEnableVertexAttribArray(positionLocation);
	glEnableVertexAttribArray(uvLocation);
	glEnableVertexAttribArray(textureLocation);
	
	glEnableVertexAttribArray(simple_positionLocation);
	glEnableVertexAttribArray(simple_colorLocation);


	texID[0] = loadTexture("stage1.raw");
	texID[1] = loadTexture("fireup.raw");
	texID[2] = loadTexture("water1.raw");
	texID[3] = loadTexture("bombup.raw");
	texID[4] = loadTexture("TNT.raw");
	texID[5] = loadTexture("1PWin.raw");
	texID[6] = loadTexture("2PWin.raw");
	texID[7] = loadTexture("DRAW.raw");
	
	//SetLighting();
	//mesh.Load("dosei.obj");
	//mesh.Information();

	//fbo.Initialize(FBOWIDTH, FBOHEIGHT);

	animtime = 0;
	
}

Image::~Image(){
	animtime = 0;
}

void Image::draw(int X, int Y, int ID) {


	// フレームバッファオブジェクトを結合する
	//glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo.GetFramebufferID());

	// ビューポートの設定
	//glViewport(0, 0, FBOWIDTH, FBOHEIGHT);
	//glViewport(0, 0, 1980, 1080);
	
	//壁
	if (ID == 0) {
		
		glUseProgram(simple_shader);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 0, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(simple_matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		glVertexAttribPointer(simple_positionLocation, 3, GL_FLOAT, false, 0, wall_vertex_position);
		glVertexAttribPointer(simple_colorLocation, 4, GL_FLOAT, false, 0, wall_color);

		glDrawElements(GL_TRIANGLES, sizeof wall_index / sizeof wall_index[0], GL_UNSIGNED_INT, wall_index);

		glUseProgram(0);
	}

	//レンガ
	if (ID == 1) {
		
		
		glUseProgram(simple_shader);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 0, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(simple_matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		glVertexAttribPointer(simple_positionLocation, 3, GL_FLOAT, false, 0, brick_vertex_position);
		glVertexAttribPointer(simple_colorLocation, 4, GL_FLOAT, false, 0, brick_color);

		glDrawElements(GL_TRIANGLES, 18, GL_UNSIGNED_INT, brick_index);

		glUseProgram(0);
		
	}

	//床
	if (ID == 2) {
		glUseProgram(texture_shader);

		glBindTexture(GL_TEXTURE_2D, texID[0]);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 0, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		drawTexture(textureLocation, positionLocation, uvLocation, texID[0], vertex_position, vertex_uv);

		glUseProgram(0);
	}

	//Unknow
	if (ID == 3) {
		glUseProgram(texture_shader);
		
		glBindTexture(GL_TEXTURE_2D, texID[0]);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 0, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		drawTexture(textureLocation, positionLocation, uvLocation, texID[1], vertex_position, vertex_uv);

		glUseProgram(0);
	}

	//爆弾アイテム
	if (ID == 4) {
		glUseProgram(texture_shader);

		glBindTexture(GL_TEXTURE_2D, texID[3]);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 1, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		drawTexture(textureLocation, positionLocation, uvLocation, texID[1], vertex_position, vertex_uv);

		glUseProgram(0);
	}

	//爆風アイテム
	if (ID == 5) {
		glUseProgram(texture_shader);

		glBindTexture(GL_TEXTURE_2D, texID[1]);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 1, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		drawTexture(textureLocation, positionLocation, uvLocation, texID[1], vertex_position, vertex_uv);

		glUseProgram(0);
	}

	//爆弾
	if (ID == 6) {
		
		glUseProgram(simple_shader);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 0, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(simple_matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		glVertexAttribPointer(simple_positionLocation, 3, GL_FLOAT, false, 0, bomb_vertex_position);
		glVertexAttribPointer(simple_colorLocation, 4, GL_FLOAT, false, 0, bomb_color);

		glDrawElements(GL_TRIANGLES, sizeof bomb_index / sizeof bomb_index[0], GL_UNSIGNED_INT, bomb_index);

		glUseProgram(0);

		glUseProgram(texture_shader);

		glBindTexture(GL_TEXTURE_2D, texID[4]);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 1, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		drawTexture(textureLocation, positionLocation, uvLocation, texID[1], bomb_tx_position, bomb_tx_uv);

		glUseProgram(0);

		
	}
	
	//横方向炎
	if (ID == 7) {
		glUseProgram(fire_shader);

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);//アルファの設定
		glEnable(GL_BLEND);//アルファのブレンド有効

		glUniform1i(useDoublefireLocation, 0);

		glBindTexture(GL_TEXTURE_2D, texID[1]);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 1, Y));
		modelMat = glm::rotate(modelMat,glm::radians(90.0f), vec3(0, 1, 0));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		glUniform1f(timeLocation3, animtime / 60);

		drawTexture(textureLocation, positionLocation, uvLocation, texID[1], vertex_position, vertex_uv);

		glUseProgram(0);

		glDisable(GL_BLEND);//アルファのブレンド有効
		animtime++;
	}
	
	//縦方向炎
	if (ID == 8) {
		glUseProgram(fire_shader);

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);//アルファの設定
		glEnable(GL_BLEND);//アルファのブレンド有効

		glUniform1i(useDoublefireLocation, 0);

		glBindTexture(GL_TEXTURE_2D, texID[1]);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 1, Y));
		modelMat = glm::rotate(modelMat, glm::radians(0.0f), vec3(0, 1, 0));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		glUniform1f(timeLocation3, animtime / 60);

		drawTexture(textureLocation, positionLocation, uvLocation, texID[1], vertex_position, vertex_uv);

		glUseProgram(0);

		glDisable(GL_BLEND);//アルファのブレンド有効
		animtime++;
	}

	//爆発中
	if (ID == 9) {
		glUseProgram(fire_shader);

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);//アルファの設定
		glEnable(GL_BLEND);//アルファのブレンド有効

		glUniform1i(useDoublefireLocation, 1);

		glBindTexture(GL_TEXTURE_2D, texID[1]);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 1, Y));
		modelMat = glm::rotate(modelMat, glm::radians(0.0f), vec3(0, 1, 0));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		glUniform1f(timeLocation3, animtime / 60);

		drawTexture(textureLocation, positionLocation, uvLocation, texID[1], vertex_position, vertex_uv);

		glUseProgram(0);

		glDisable(GL_BLEND);//アルファのブレンド有効
		animtime++;
	}

	if (ID == 10) {
		glUseProgram(simple_shader);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 0, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(simple_matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		glVertexAttribPointer(simple_positionLocation, 3, GL_FLOAT, false, 0, brick_vertex_position);
		glVertexAttribPointer(simple_colorLocation, 4, GL_FLOAT, false, 0, burnbrick_color);

		glDrawElements(GL_TRIANGLES, 18, GL_UNSIGNED_INT, brick_index);

		glUseProgram(0);

	}

	//1P
	if (ID == 20) {
		glUseProgram(simple_shader);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 0, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(simple_matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		glVertexAttribPointer(simple_positionLocation, 3, GL_FLOAT, false, 0, oneP_position);
		glVertexAttribPointer(simple_colorLocation, 4, GL_FLOAT, false, 0, oneP_color);

		glDrawElements(GL_TRIANGLES, 18, GL_UNSIGNED_INT, oneP_index);

		glUseProgram(0);
	}

	//2P
	if (ID == 21) {
		glUseProgram(simple_shader);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 0, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(simple_matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		glVertexAttribPointer(simple_positionLocation, 3, GL_FLOAT, false, 0, twoP_position);
		glVertexAttribPointer(simple_colorLocation, 4, GL_FLOAT, false, 0, twoP_color);

		glDrawElements(GL_TRIANGLES, 18, GL_UNSIGNED_INT, twoP_index);

		glUseProgram(0);
	}

	//ENEMY
	if (ID == 22) {
		glUseProgram(simple_shader);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 0, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(simple_matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		glVertexAttribPointer(simple_positionLocation, 3, GL_FLOAT, false, 0, player_vertex_position);
		glVertexAttribPointer(simple_colorLocation, 4, GL_FLOAT, false, 0, player_color);

		glDrawElements(GL_TRIANGLES, 18, GL_UNSIGNED_INT, player_index);

		glUseProgram(0);
	}
	
	
	/*
	// フレームバッファオブジェクトを結合する
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	glUseProgram(texture_shader);

	// ビューポートはウィンドウのサイズに合わせる
	glViewport(0, 0, width, height);

	// attribute属性を登録
	glVertexAttribPointer(positionLocation, 3, GL_FLOAT, false, 0, f_position);
	glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 0, f_uv);

	//glUniform1f(timeLocation2, animtime/10000);

	//modelMat = glm::rotate(glm::mat4(1.0f), 0.0f, vec3(0, 1, 0));
	modelMat = glm::translate(glm::mat4(1.0f), vec3(0, 0, 0));
	mvpMat = projectionMat * orthoMat1 * modelMat;

	glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

	// モデルの描画
	glBindTexture(GL_TEXTURE_2D, fbo.GetColorbufferID());
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	glUseProgram(0);

	animtime++;
	*/
	
		
};

void Image::drawcleared(int ID) {
	glUseProgram(texture_shader);

	if (ID == 0) {
		glBindTexture(GL_TEXTURE_2D, texID[5]);
	}

	if (ID == 1) {
		glBindTexture(GL_TEXTURE_2D, texID[6]);
	}

	if (ID == 2) {
		glBindTexture(GL_TEXTURE_2D, texID[7]);
	}

	// attribute属性を登録
	glVertexAttribPointer(positionLocation, 3, GL_FLOAT, false, 0, f_position);
	glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 0, f_uv);

	modelMat = glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), vec3(1, 0, 0));
	modelMat = glm::translate(modelMat, vec3(0, 0, 0));
	mvpMat = projectionMat * orthoMat1 * modelMat;

	glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	glUseProgram(0);
}

