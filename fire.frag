#version 120
//
// shader.frag
//
varying vec2 vuv;
uniform float time;
uniform sampler2D texture;

uniform int x;

float metaball(vec2 pos, vec2 offset, float scale){
    // ポジション修正
    pos = pos - offset;
    float len = length(pos);
    float draw = 0.0;
    // 円を描く範囲
    if(len < scale){
        draw = (1.0 - len / scale);
    }
    return draw;
}

// 各ピクセルの原点からの距離を計測し色として出力する
void main(){
    // gl_FragCoord.xy = 0?512 → *2.0で0?1024 → -resoで-512?512 → /resoで-1.0?1.0座標
    vec2 p = vuv * 2.0 - 1.0;
    float ball = 0.0;
    float speed = 10.0; // 炎のスピード
	 float xPos = 0.8;   // Y軸の調整
    float yPos = 0.8;   // Y軸の調整

	
    // 横揺れの炎8つ
for(int i = 1; i <= 8; i++){
    float x = float(i) * 1.0;
    float y = float(8-i) * 0.18;
    float moveX = cos(x+time*speed) * 0.11;
    float moveY = y - yPos;
    // moveX = 0.0;
    // moveY = 0.0;
    float scale = 0.56;
    // メタボール生成
    ball += metaball(p, vec2(moveX, moveY), scale);
}


// 縦揺れの炎8つ
for(int i = 1; i <= 8; i++){
    float x = float(i) * 0.0;
    float y = float(i) * 0.22;
    float moveX = x;
    float moveY = y - yPos  + sin(float(i)+time*speed) * 0.2;
    // moveX = 0.0;
    // moveY = 0.0;
    float scale = 0.4;
    // メタボール生成
    ball += metaball(p, vec2(moveX, moveY), scale);
}

if(x == 1){
// 縦揺れの炎8つ
for(int i = 1; i <= 8; i++){
    float x = float(8-i) * 0.22;
    float y = 0;
    float moveX = x - xPos + sin(float(i)+time*speed) * 0.2;
    float moveY = 0.0 ;
    // moveX = 0.0;
    // moveY = 0.0;
    float scale = 0.4;
    // メタボール生成
    ball += metaball(p, vec2(moveX, moveY), scale);
}


  // 横揺れの炎8つ
for(int i = 1; i <= 8; i++){
    float x = float(8-i) * 0.22;
    float y = 0;
    float moveX = x - xPos;
    float moveY = cos(y+time*speed) * 0.11;
    // moveX = 0.0;
    // moveY = 0.0;
    float scale = 0.56;
    // メタボール生成
    ball += metaball(p, vec2(moveX, moveY), scale);
}
}

// red color

	vec4 color = vec4(vec3(ball, ball*0.45+ball*0.1,ball*0.25+ball*0.1 ), 1.0);

	if(color.r>0.4){
		gl_FragColor = vec4(vec3(ball, ball*0.45+ball*0.1,ball*0.25+ball*0.1 ), 1.0);
	}else{
		gl_FragColor = vec4(vec3(ball, ball*0.45+ball*0.1,ball*0.25+ball*0.1 ), 0.0);
	}


	
}